import React, { Component } from 'react';
import className from 'classnames';
import { Link } from 'react-router-dom';
class LaunchItem extends Component {
  state = {};
  render() {
    return (
      <div className="card card-body mb-3">
        <div className="row shadow p-3 mb-5 bg-white rounded ">
          <div className="col-md-9 launch  ">
            <div>
              <h4>
                Mission:{' '}
                <span
                  className={className({
                    'text-success': this.props.launch.launch_success,
                    'text-danger': !this.props.launch.launch_success
                  })}
                >
                  {this.props.launch.mission_name}
                </span>
              </h4>
              <p>Date:{this.props.launch.launch_date_local}</p>
            </div>

            <div className="col-md-3">
              <Link to={`/launch/${this.props.launch.flight_number}`}>
                <button className="btn btn-secondary"> Launch Details</button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LaunchItem;
