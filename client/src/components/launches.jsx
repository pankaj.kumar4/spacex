import React, { Component, Fragment } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import LaunchItem from './LaunchItem'

const LAUNCHES_QUERY = gql`
  query LaunchesQuery {
    launches {
      flight_number
      mission_name
      launch_date_local
      launch_success
    }
  }
`;
class Launches extends Component {
  state = {};
  render() {
    return (
      <div>
        <h4>Launches</h4>
        <Query query={LAUNCHES_QUERY}>
          {({ loading, error, data }) => {
            if (loading) return <h4>Loading...</h4>;
            if (error) console.log(error); 
            return (<Fragment>
                {
                    
                    data.launches.map(launch=>(<LaunchItem launch={launch} key={launch.flight_number}/>))
                }
            </Fragment>)
          }}
        </Query>
      </div>
    );
  }
}

export default Launches;
